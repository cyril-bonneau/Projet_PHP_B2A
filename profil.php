<?php

require 'connectdb.php';
require 'Includes/expiration_session.php';

session_start();
if (!$_SESSION['id']) {
    echo "<script language='JavaScript'>document.location='connexion.php'</script>";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mon profil</title>
    <?php require 'Includes/head.php'?>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}

$id = $_SESSION['id'];
$requete = $con->query("SELECT * FROM user WHERE ID = '$id'");
$informations = $requete->fetch()
?>
<article id="arti" class="ui piled segment">
    <h1 style="font-size: 26px; text-align: center">Bonjour <?php echo $informations['surname']?>!</h1>
    <hr style="width: 50%; margin-bottom: 50px">
    <div id="divg">
        <h2>Mes informations personnelles :</h2>
        <br>
        <div id="profi">
            <p><strong>Prénom : </strong><?php echo $informations['surname'] ?></p>
            <p><strong>Nom : </strong><?php echo $informations['name'] ?></p>
            <p><strong>Email : </strong><?php echo $informations['email'] ?></p>
            <p><strong>Adresse : </strong><?php echo $informations['adress'] ?></p>
            <p><strong>Code Postal : </strong><?php echo $informations['zipcode'] ?></p>
            <p><strong>Ville : </strong><?php echo $informations['city'] ?></p>
        </div>
        <br>
        <a href="modification_profil.php"><input type="submit" name="button" class="ui button" value="Modifier mes informations"></a>
    </div>
    <br>
    <div id="divd">
        <h2>Mes articles mis en vente :</h2>
        <br>
        <?php

        $rockete = $con->query("SELECT * FROM products WHERE seller = '$id'");

        while ($don = $rockete->fetch()) { ?>

            <table class="ui celled table">
                <tbody>
                <tr>
                    <td style="width: 14%"><img src="<?php print $don['image']?>" style="width: 50px" height="auto"></td>
                    <td><?php echo $don['title']?></td>
                    <td style="width: 16%; font-size: 80%"><?php echo $don['price'] ." €"?></td>
                    <?php $id_produit = $don['id']?>
                    <td style="width: 17%"><a class='liens' href="fiche_produit.php?param=<?php echo $id_produit;?>">Voir plus</a></td>
                </tr>
                </tbody>
            </table>

            <?php
            } $rockete->closeCursor();
            ?>
        <br>
    </div>
</article>

<?php include 'Includes/footer.php'?>

</body>
</html>