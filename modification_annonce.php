<?php

require 'Includes/expiration_session.php';

session_start();

require 'connectdb.php';

$id_produits = $_GET['param'];
$recup_annonce = $con->query("SELECT * FROM products WHERE id='$id_produits'");
$prdt = $recup_annonce->fetch();

if (!$_SESSION['id']) {
    echo "<script language='JavaScript'>document.location='connexion.php'</script>";
} else if ($prdt['seller'] != $_SESSION['id']) {
    echo "<script language='JavaScript'>document.location='boutique.php'</script>";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Modification d'annonce</title>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}

require 'Includes/expiration_session.php';

?>

<article  id="arti" class="ui piled segment">
    <h1 style="font-size: 26px">Modifier mon annonce</h1>
    <hr style="margin-bottom: 20px">
    <br>
    <div class="dv">
        <form action="" method="post" onsubmit="return verifForm(this)" class="ui form">
            <label for="">Titre</label>
            <input type="text" name="title" id="form" class="field" maxlength="33" style="margin-bottom: 10px" value="<?php echo $prdt['title']?>">
            <br>
            <label for="">Description</label>
            <textarea name="description" id="form" class="field" style="margin-bottom: 10px" cols="30" rows="10"><?php echo $prdt['description']?></textarea>
            <br>
            <label for="">Prix</label>
            <input type="number" name="price" id="form" class="field" style="margin-bottom: 10px" value="<?php echo $prdt['price']?>">
            <br>
            <?php echo $prdt['image']?>
            <div style="margin-top: 10px; margin-bottom: 20px">
                <label for="file" class="ui icon button">
                    <i class="file icon"></i>
                    Ajouter une image à mon annonce</label>
                <input type="file" id="file" name="img" style="display:none">
            </div>
            <input type="submit" id="bouton" class="ui button" value="Valider" name="button">
            <a href="fiche_produit.php?param=<?php echo $prdt['id']?>"><input type="submit" class="ui button" value="Annuler"></a>
        </form>
    </div>
    <br>
</article>
<br>
<?php include 'Includes/footer.php'?>

<?php

$title = $_POST['title'];
$desc = $_POST['description'];
$price = $_POST['price'];
$img = $_FILES['img']['name'];
$id_annonce = $prdt['id'];

if (isset($_POST['button'])) {
    if (!empty($title) && !empty($desc) && !empty($price)) {
        $requete = $con->query("UPDATE products SET title = '$title', description = '$desc', price = '$price' WHERE id = '$id_annonce'");
        echo "<script language='Javascript'>document.location='profil.php'</script>";
    } else {
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">";
        exit('Tous les champs ne sont pas renseignés !');
        echo "</div>";
    }
}
?>

</body>
</html>