<?php
require 'connectdb.php';
require 'Includes/expiration_session.php';

session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Boutique</title>
    <?php require 'Includes/head.php'?>
</head>
<body id="body">

<?php
if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<h1 style="text-align: center">Bienvenue sur notre boutique en ligne</h1>
<hr style="width: 50%; margin-bottom: 50px">

<?php
$requ = $con->query("SELECT * FROM products");

echo '<div id="shop" class="ui stackable four column grid">';

while ($produits = $requ->fetch()) { ?>

        <div id="shop_cases" class="column" style="width: 24%; margin-left: 10px">
            <h3><?php echo $produits['title']?></h3>
            <div style="height: 250px">
                <img src="<?php print $produits['image']?>" style="max-width: 100%; max-height: 250px">
            </div>
            <br>
            <p style="border: solid 1px black; padding: 5px 10px; display: inline"><?php echo $produits['price']?> €</p>
            <br>
            <?php $id_produit = $produits['id']?>
            <a href="fiche_produit.php?param=<?php echo $id_produit;?>""><input style="margin-top: 20px" type="submit" name="button" id="bouton" class="ui button" value="Voir l'annonce"></a>
        </div>
    <?php
    }
    $requ->closeCursor();
    ?>
</div>
</body>
</html>
