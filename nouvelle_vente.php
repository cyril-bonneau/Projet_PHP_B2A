<?php

require 'connectdb.php';
require 'Includes/expiration_session.php';

session_start();
if (!$_SESSION['id']) {
    echo "<script language='JavaScript'>document.location='connexion.php'</script>";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php require 'Includes/head.php'?>
    <title>Mettre un produit en vente</title>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<article  id="arti" class="ui piled segment">
    <form action="" method="post" class="ui form" enctype="multipart/form-data">
        <div class="aligncenter">
            <h1 style="font-size: 26px; margin-bottom: 10px">Vendre ma peluche</h1>
            <hr style="margin-bottom: 30px">
            <label for="">Titre de votre annonce (< 33 caractères)</label>
            <input type="text" id="form"  name="title" class="field" placeholder="Titre" maxlength="33" style="margin-bottom: 10px">
            <label for="">Description du produit</label>
            <textarea name="description" id="form" cols="30" rows="10" class="field" placeholder="Description"></textarea>
            <label for="">Prix</label>
            <input type="number" id="form" name="price" class="field" style="margin-bottom: 10px" placeholder="Prix">
            <div style="margin-bottom: 20px">
                <label for="file" class="ui icon button">
                    <i class="file icon"></i>
                    Ajouter une image à mon annonce</label>
                <input type="file" id="file" name="img" style="display:none">
            </div>
            <p style="color: darkgrey; padding-bottom: 10px">Ajouter une image facilitera la vente de votre produit.</p>
            <input type="submit" id="bouton" class="ui button" name="sub" value="Valider">
        </div>
    </form>
</article>


<?php

$title = $_POST['title'];
$desc = $_POST['description'];
$price = $_POST['price'];

if (isset($_POST['sub'])) {
    if (!empty($title) && !empty($desc) && !empty($price)) {
        $req = $con->prepare('INSERT INTO products (id, price, title, description, image, seller) VALUES (NULL, ?, ?, ?, ?, ?)');
        $req->execute(array($price, $title, $desc, 'Uploads/'.$_FILES['img']['name'], $_SESSION['id']));
        move_uploaded_file($_FILES['img']['tmp_name'], 'Uploads/'.$_FILES['img']['name']);
        echo "<script language='JavaScript'>document.location='profil.php'</script>";
    } else {
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">";
        exit('Tous les champs ne sont pas renseignés !');
        echo "</div>";
    }
}
?>
<br>
<?php include 'Includes/footer.php'?>
</body>
</html>
