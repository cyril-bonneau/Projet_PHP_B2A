<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?php require 'Includes/head.php'?>
        <title>Document</title>
    </head>
    <body>

    <?php

    require 'connectdb.php';

    if ($_SESSION['email']) {
        include 'Includes/menu2.php';
    } else {
        include 'Includes/menu1.php';
    }
    ?>
        <h1 style="font-size: 26px; margin-left: 100px">Inscription</h1>
        <article id="arti" class="ui piled segment">
            <div class="align">
                <form action="" method="post" class="ui form" onsubmit="return verifForm(this)">
                    <label>Nom</label>
                    <br>
                    <div class="margin">
                        <input name="name" type="text" id="form" placeholder="Prénom" class="field" style="margin-bottom: 10px">
                        <input name="surname" type="text" id="form" placeholder="Nom" class="field">
                    </div>
                    <br>
                    <label>Identifiants de connexion</label>
                    <br>
                    <div class="margin">
                        <input name="email" type="email" id="form" placeholder="Email" class="field" style="margin-bottom: 10px" onblur="verifEmail(this)">
                        <input name="password" type="password" id="form" placeholder="Mot de passe" class="field" onblur="verifPassword(this)">
                    </div>
                    <br>
                    <label>Informations de livraison</label>
                    <br>
                    <div class="margin">
                        <input name="adress" type="text" id="form" placeholder="Adresse" class="field" style="margin-bottom: 10px">
                        <input name="zipcode" type="text" id="form" placeholder="Code postal" class="field" style="margin-bottom: 10px">
                        <input name="city" type="text" id="form" placeholder="Ville" class="field">
                    </div>
                    <br>
                    <input name="bouton" type="submit" class="ui button" value="Valider">
                </form>
            </div>
        </article>

    <?php

    require 'connectdb.php';

    if(isset($_POST['bouton'])) {
        if (!empty($_POST['name']) && !empty($_POST['surname']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['adress']) && !empty($_POST['zipcode']) && !empty($_POST['city'])) {
            $req = $con->prepare('SELECT email FROM user WHERE email = :email');
            $req->execute([
                'email' => $_POST['email'],
            ]);
            $donnes = $req->fetch();
            if (!$donnes) {
                $requete = $con->prepare('INSERT INTO user (ID, name, surname, email, password, zipcode, city, adress) VALUES(NULL, :name, :surname, :email, :password, :zipcode, :city, :adress)');
                $requete->execute([
                    ':name' => $_POST['name'],
                    ':surname' => $_POST['surname'],
                    ':email' => $_POST['email'],
                    ':password' => $_POST['password'],
                    ':zipcode' => $_POST['zipcode'],
                    ':city' => $_POST['city'],
                    ':adress' => $_POST['adress']
                ]);
                session_start();
                echo "<script language='JavaScript'>document.location='index.php'</script>";
            } else {
                echo "<div style=\"text-align : center;margin-bottom:20px;color:red\">";
                exit('Cette adresse email est déjà utilisée. <a href="connexion.php" class="btn btn-primary" role="button">Me connecter</a>');
                echo "</div>";
            }
        } else {
            echo "<div style=\"text-align : center;margin-bottom:20px;color:red\">";
            exit('Tous les champs n\'ont pas été renseignés !');
            echo "</div>";
        }
    }
    ?>

        <script type="text/javascript">
            function surligne(champ, erreur)
            {
                if(erreur)
                    champ.style.backgroundColor = "#fba";
                else
                    champ.style.backgroundColor = "";
            }
            function verif(champ)
            {
                if(champ.value.length < 1)
                {
                    surligne(champ,true);
                    return false;
                }
                else
                {
                    surligne(champ,false)
                    return true;
                }
            }
            function verifNom(champ)
            {
                var regex = /^[a-zA-Z]+$/
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifPrenom(champ)
            {
                var regex = /^[a-zA-Z]+$/
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifEmail(champ)
            {
                var regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/;
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifPassword(champ)
            {
                var regex = /^[a-zA-Z0-9]{6,}$/;
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifCP(champ)
            {
                var regex = /^[0-9]{2,}$/
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifAdresse(champ)
            {
                var regex = /^[a-zA-Z]+$/
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifVille(champ)
            {
                var regex = /^[a-zA-Z]+$/
                if(!regex.test(champ.value))
                {
                    surligne(champ, true);
                    return false;
                }
                else
                {
                    surligne(champ, false);
                    return true;
                }
            }
            function verifForm(f)
            {
                var nomOK = verifNom(f.nom);
                var prenomOK = verifPrenom(f.prenom);
                var EmailOK = verifEmail(f.email);
                var passwordOK = verifPassword(f.password);
                var adresseOK = verifAdresse(f.adress);
                var CPOK = verifCP(f.zipcode);
                var VilleOK = verifVille(f.city);

                if(nomOK && prenomOK && EmailOK && passwordOK && adresseOK && CPOK && VilleOK)
                {
                    return true;
                }
                else
                {
                    alert("Tous les champs ne sont pas correctement remplis !")
                    return false;
                }
            }
        </script>

        <?php require 'Includes/footer.php'?>
    </body>
</html>
