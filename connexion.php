<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Connexion</title>
</head>
<body id="body">

<?php include 'Includes/menu1.php'?>

<article id="arti" class="ui piled segment">
    <div class="aligncenter">
    <form action="" method="post" class="ui form">
        <h1 style="font-size: 26px">Connexion</h1>
        <hr style="width: 100%; margin-bottom: 10px">
        <br>
        <label for="">Adresse email</label>
        <input type="email" id="form" class="field" name="id" style="margin-bottom: 10px" placeholder="Email">
        <br>
        <label for="">Mot de passe</label>
        <input type="password" id="form" class="field" name="password" style="margin-bottom: 10px" placeholder="Mot de passe">
        <br>
        <input style="margin-top: 20px" type="submit" id="bouton" class="ui button" name="sub" value="Valider"> <p style="margin-top: 20px;">Pas encore membre ? Inscrivez-vous <a href="inscription.php" class="liens"> ici.</a></p>
        <a href="mdp_oublie.php" class="liens">J'ai oublié mon mot de passe.</a>
    </form>
    </div>
</article>
<br>

<?php

require 'connectdb.php';

if (isset($_POST['sub'])) {
    if(!empty($_POST['id'] && !empty($_POST['password'])))
    {
        $req = $con->prepare('SELECT ID FROM user WHERE email = :email AND password = :pass');
        $req->execute([
            'email' => $_POST['id'],
            'pass' => $_POST['password']
        ]);
        $donnees = $req->fetch();
        if (!$donnees) {
            echo "<div class=\'alert'\">";
            exit('Vos identifiants sont incorrects.');
            echo "</div>";
        } else {
            session_start();
            $_SESSION['id'] = $donnees['ID'];
            $_SESSION['start'] = time();
            $_SESSION['expire'] = $_SESSION['start'] + (60 * 60);
            echo "<script language='JavaScript'>document.location='index.php'</script>";
        }
    }
    else
    {
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">";
        exit('Tous les champs n\'ont pas été renseignés !');
        echo "</div>";
    }
}
?>
<footer style="position: absolute; bottom: 0; left: 0; right: 0">
    <div style="display: inline-block; margin-top: 15px; width: 100%">
        <div style="float: right; padding-right: 20px">
            <p style="color: white; float: right">Papuche © 2018</p>
        </div>
        <p style="float: left">
            <img src="Images/fbclair.png" style="height: 20px; width: 20px">
            <img src="Images/twitclair.png" alt="" style="height: 20px; width: 20px">
            <img src="Images/instaclair.png" alt="" style="height: 20px; width: 20px">
        </p>
    </div>
</footer>
</body>
</html>
