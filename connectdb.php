<?php
session_start();

$dsn = 'mysql:dbname=papuche;host=localhost';
$user = 'root';
$password = 'root';

try {
    $con = new PDO($dsn, $user, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Échec lors de la connexion : ' . $e->getMessage();
}
?>