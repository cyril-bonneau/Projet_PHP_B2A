<?php

require 'connectdb.php';
require 'Includes/expiration_session.php';

session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article en vente</title>
    <?php require 'Includes/head.php'?>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<div id="arti" class="ui piled segment">

<?php

    $id_produits = $_GET['param'];
    $recup_annonce = $con->query("SELECT * FROM products WHERE id='$id_produits'");

while ($prdt = $recup_annonce->fetch()) { ?>

    <h3>Annonce <?php echo $prdt['title']?></h3>
    <div style="height: auto; float: left; width: 45%">
        <img src="<?php print $prdt['image']?>" style="max-width: 100%; max-height: 400px">
    </div>
    <div style="float: right; border: solid 1px black; padding: 5px 10px; display: inline; width: 45%">
        <p><?php echo $prdt['description']?></p>
        <hr style="margin-bottom: 10px">
        <p>Prix : <?php echo $prdt['price']?> €</p>
        <?php

        $id_produit = $prdt['id'];
        if ($prdt['seller'] != $_SESSION['id']) {
            ?>
            <a href="achat.php?param=<?php echo $id_produits;?>"><input type="submit" id="bouton" class="ui button" value="Acheter"></a>
            <?php
        } else {
            ?>
            <input type="submit" name="delete" id="bouton" class="ui button" value="Supprimer">
        <?php
        }
        if (isset($_POST['delete'])) {
            var_dump($_POST['delete']);
            global $con;
            $suppr_annonce = $con->exec("DELETE * FROM products WHERE id='$id_produit'");
        }
        ?>

        <?php

        $id_produit = $prdt['id'];
        if ($prdt['seller'] == $_SESSION['id']) {
            ?>
            <a href="modification_annonce.php?param=<?php echo $id_produit;?>"><input type="submit" id="bouton" class="ui button" value="Modifier l'annonce"></a>
            <?php
        }
        ?>

        <a href="boutique.php"><input type="submit" class="ui button" value="Retour"></a>
    </div>

    <?php
    } $recup_annonce->closeCursor();
    ?>
</div>
<br>
<footer style="position: absolute; bottom: 0; left: 0; right: 0">
    <div style="display: inline-block; margin-top: 15px; width: 100%">
        <div style="float: right; padding-right: 20px">
            <p style="color: white; float: right">Papuche © 2018</p>
        </div>
        <p style="float: left">
            <img src="Images/fbclair.png" style="height: 20px; width: 20px">
            <img src="Images/twitclair.png" alt="" style="height: 20px; width: 20px">
            <img src="Images/instaclair.png" alt="" style="height: 20px; width: 20px">
        </p>
    </div>
</footer>
</body>
</html>