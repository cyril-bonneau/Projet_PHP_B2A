<?php

require 'connectdb.php';
require 'Includes/expiration_session.php';

session_start();
if (!$_SESSION['id']) {
    echo "<script language='JavaScript'>document.location='connexion.php'</script>";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article en vente</title>
    <?php require 'Includes/head.php'?>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<article class="ui piled segment" style="margin-left: 10%; margin-right: 10%; padding-top: 60px; padding-bottom: 60px">
    <h1 style="text-align: center">Acheter</h1>
    <hr style="width: 60%">
    <br>
    <div class="align">
        <form action="" method="post" class="ui form">
            <div class="field">
                <label>Type de carte</label>
                <select type="hidden" class="ui fluid search dropdown" name="card[type]">
                    <option value="0">Type</option>
                    <option value="1">Visa</option>
                    <option value="2">American Express</option>
                    <option value="3">MasterCard</option>
                </select>
            </div>
        <div class="fields">
            <div class="seven wide field">
                <label>N° de carte bancaire</label>
                <input type="text" name="card[number]" maxlength="16" placeholder="Carte #">
            </div>
            <div class="three wide field">
                <label>CVV</label>
                <input type="text" name="card[cvc]" maxlength="3" placeholder="CVV">
            </div>
            <div class="six wide field">
                <label>Date d'expiration</label>
                <div class="two fields">
                    <div class="field">
                        <select class="ui fluid search dropdown" name="card[expire-month]">
                            <option value="0">Mois</option>
                            <option value="1">Janvier</option>
                            <option value="2">Fevrier</option>
                            <option value="3">Mars</option>
                            <option value="4">Avril</option>
                            <option value="5">Mai</option>
                            <option value="6">Juin</option>
                            <option value="7">Juillet</option>
                            <option value="8">Août</option>
                            <option value="9">Septembre</option>
                            <option value="10">Octobre</option>
                            <option value="11">Novembre</option>
                            <option value="12">Décembre</option>
                        </select>
                    </div>
                    <div class="field">
                        <input type="text" name="card[expire-year]" maxlength="4" placeholder="Année">
                    </div>
                </div>
            </div>
        </div>
            <input type="submit" name="purchase" class="ui button" id="bouton" value="Confirmer">
    </div>
    </form>
</article>

<?php

$type = intval($_POST['card']['type']);
$num = $_POST['card']['number'];
$cvv = $_POST['card']['cvc'];
$date_month = intval($_POST['card']['expire-month']);
$date_year = $_POST['card']['expire-year'];
$year = date('Y');

if (isset($_POST['purchase'])) {
    if ($type > 0  && $type <= 3 && !empty($num) && !empty($cvv) && $date_month > 0 && $date_month <= 12 && !empty($date_year)) {
        if ($date_year >= $year) {
            require 'verif_payement.php';
            luhn_validate($num);
            if (luhn_validate($num) == true) {
                $id_produit = $_GET['param'];
                global $con;
                $suppr_annonce = $con->exec("DELETE * FROM products WHERE id='$id_produit'");
            } else {
                exit("<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">Le numéro de carte bancaire saisi est invalide</div>");
            }
        } else {
            exit("<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">L'année d'expiration est invalide</div>");
        }
    } else {
        exit("<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">Tous les champs n'ont pas été renseignés</div>");
    }
}
?>

<footer style="position: absolute; bottom: 0; left: 0; right: 0">
    <div style="display: inline-block; margin-top: 15px; width: 100%">
        <div style="float: right; padding-right: 20px">
            <p style="color: white; float: right">Papuche © 2018</p>
        </div>
        <p style="float: left">
            <img src="Images/fbclair.png" style="height: 20px; width: 20px">
            <img src="Images/twitclair.png" alt="" style="height: 20px; width: 20px">
            <img src="Images/instaclair.png" alt="" style="height: 20px; width: 20px">
        </p>
    </div>
</footer>
</body>
</html>