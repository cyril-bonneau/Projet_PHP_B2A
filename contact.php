<?php

require 'Includes/expiration_session.php';
require 'connectdb.php'
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Nous retrouver</title>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<article  id="arti" class="ui piled segment">
    <h1 style="font-size: 29px">Où sommes-nous ?</h1>
    <p>12 rue Anatole France <br>92000 Nanterre</p>
    <h2 style="font-size: 26px; margin-bottom: 20px">Réseaux sociaux</h2>
    <img src="Images/fbclair.png" style="height: 45px; width: 45px">
    <img src="Images/twitclair.png" alt="" style="height: 45px; width: 45px">
    <img src="Images/instaclair.png" alt="" style="height: 45px; width: 45px">
    <br>
    <h3 style="margin-top: 20px; margin-bottom: 15px">Nous contacter via le support</h3>
    <div class="align" style="margin-left: 0">
        <form action="https://formspree.io/alexandra.fabre@ynov.com" method="post" class="ui form">
            <br>
            <input type="text" name="name" class="field" placeholder="Nom" style="margin-bottom: 10px">
            <br>
            <input type="email" name="adresseemail" class="field" placeholder="Adresse email" style="margin-bottom: 10px">
            <br>
            <input type="text" name="subject" class="field" placeholder="Sujet" style="margin-bottom: 10px">
            <br>
            <textarea name="message" cols="30" rows="10" placeholder="Message" class="field" style="margin-bottom: 10px"></textarea>
            <br>
            <input type="submit" id="bouton" class="ui button" value="Envoyer" name="send">
        </form>
    </div>
</article>

<?php include 'Includes/footer.php'?>

<?php

if (isset($_POST['send'])) {
    if (!empty($_POST['name']) && !empty($_POST['adresseemail']) && !empty($_POST['subject']) && !empty($_POST['message'])) {
        $requete = $con->prepare("INSERT INTO messages(ID, sender_name, sender_email, subject, message) VALUES (NULL, ?, ?, ?, ?)");
        $requete->execute(array($_POST['name'], $_POST['adresseemail'], $_POST['subject'], $_POST['adresseemail']));
        echo "</div>";
        $message  = ''.$_POST['message'].'
        
        '.$_POST['adresseemail'].'
        '.$_POST['name'].'';

        mail('alexandra.fabre@ynov.com', $_POST['subject'], $_POST['message']);
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: black;'\">";
        echo "Votre message a bien été envoyé !";
        echo "</div>";
    } else {
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">";
        exit('Veuillez remplir tous les champs.');
        echo "</div>";
    }
}
?>

</body>
</html>
