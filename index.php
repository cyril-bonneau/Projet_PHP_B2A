<?php
require 'connectdb.php';
require 'Includes/expiration_session.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Accueil</title>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>
<h1 style="text-align: center">Bienvenue sur Papuche !</h1>
<br>
<div class="ui equal width grid" style="margin-left: 10px; margin-right: 10px">
    <div class="column"><img class="ui rounded large centered image" src="Images/happy-puppy-108997.jpeg" ></div>
    <div class="column"><img class="ui rounded large centered image" src="Images/TEST.jpg" ></div>
    <div class="column"><img class="ui rounded large centered image" src="Images/happy-puppy-108997.jpeg" ></div>
</div>

<div class="ui piled segment" style="margin-right: 20px; margin-left: 20px">
    <div class="ui segment" style="text-align: center">
        <h3>Donnez une seconde vie à nos peluches !</h3>
    </div>
    <div class="ui segment">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam,
            feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.
            Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.
            Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
    </div>
    <div class="ui segment">
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam,
            feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est.
            Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.
            Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>    </div>
</div>

</body>
</html>

<?php include 'Includes/footer.php'?>

</body>
</html>
