<?php require 'connectdb.php'?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Mot de passe oublié</title>
</head>
<body id="body">

<?php

if ($_SESSION['id']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<article  id="arti" class="ui piled segment">
    <h1 style="font-size: 26px">Récupérer mon mot de passe</h1>
    <p>Votre mot de passe vous a bien été envoyé par mail.</p>
    <br>
    <a href="index.php" class="liens">Retour à l'accueil</a>
</article>

<?php include 'Includes/footer.php'?>

</body>
</html>
