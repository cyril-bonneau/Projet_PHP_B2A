<?php

function luhn_validate($num, $mod5 = false) {
$parity = strlen($num) % 2;
$total = 0;
$digits = str_split($num);
foreach($digits as $key => $digit) {
    if (($key % 2) == $parity)
$digit = ($digit * 2);
if ($digit >= 10) {
$digit_parts = str_split($digit);
$digit = $digit_parts[0]+$digit_parts[1];
}
$total += $digit;
}
return ($total % ($mod5 ? 5 : 10) == 0 ? true : false);
}
?>