<?php require 'connectdb.php' ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include 'Includes/head.php'?>
    <title>Mot de passe oublié</title>
</head>
<body id="body">

<?php

if ($_SESSION['email']) {
    include 'Includes/menu2.php';
} else {
    include 'Includes/menu1.php';
}
?>

<article class="art">
    <h1 style="font-size: 26px; margin-left: 20px">Récupérer mon mot de passe</h1>
    <div class="align">
        <form action="" method="post" class="ui form">
            <input type="email" id="form" name="email" class="field" style="margin-bottom: 10px" placeholder="Email">
            <br>
            <input type="submit" id="form" name="button" class="ui button" value="Envoyer">
            <a href="connexion.php" class="">Retour à la connexion</a>
        </form>
    </div>
</article>

<?php include 'Includes/footer.php'?>

<?php

$mailto = $_POST['email'];
$email = $_SESSION['email'];

if (isset($_POST['button'])) {
    if (!empty($mailto)) {
        $requete = $con->prepare("SELECT password FROM user WHERE email = '$email'");
        $requete->execute(array('email' => $mailto));
        $password = $requete->fetch();
        if (!$password) {
            echo "<div style=\"text-align : center;margin-bottom:20px;color:darkred;'\">";
            exit('L\'adresse email saisie n\'est pas enregistrée.');
            echo "</div>";
        } else {
            echo "</div>";
            $message  = 'Suite à votre demande, voici votre mot de passe : '.$password['password'].'';
            mail($mailto, 'Votre mot de passe Papuche', $message);
            echo "<script language='JavaScript'>document.location='redirection_mdp_oublie.php'</script>";
        }
    } else {
        echo "<div style=\"text-align: center; margin-bottom: 20px; color: darkred;'\">";
        exit('Veuillez saisir votre adresse email.');
        echo "</div>";
    }
}
?>

</body>
</html>